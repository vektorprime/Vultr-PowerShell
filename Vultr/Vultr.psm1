#Created by Vic @ http://techstat.net

function Get-VultrVM {
     <#
.Synopsis
   Gets basic information about your virtual machines at vultr.com
.DESCRIPTION
   This function will be your most used in the module. It creates objects that you will later pass on to other functions like Restart-VultrVM or Remove-VultrVM.
   The best way to use this function is to delcare your API Key in a variable. 
   This function get's the VM: Name, OS, CPU, Ram, Disk, location, power, Subid and it's API Key.
   the reason we also get the API key is so we can pass it along the pipeline to other functions.
.EXAMPLE
   This will get all your VMs in Vultr.
   Get-VultrVM -API $apivar
.EXAMPLE
   Get-VultrVM -name 'VM 1','VM 2' -API $apivar
#>
    [cmdletbinding()]
    Param(
         [Parameter(mandatory = $true,
                    parametersetname = 'AllVMs')]
         [Parameter(parametersetname = 'SingleVM')]
         $api,
         [parameter(Mandatory = $true,
                    parametersetname = 'SingleVM',
                    position = 0)]
         [array]$Name
     )#param
     

     
         
    switch ($PSCmdlet.ParameterSetName) {
    'AllVMs' {
        $allvms = invoke-webrequest -uri https://api.vultr.com/v1/server/list?api_key=$api -Method get -SessionVariable vultr
        $vminfo = $allvms.Content -split ',' 
        $vminfo2 = $vminfo.replace("`"",'')
        [System.Collections.ArrayList]$vminfofiltered =  $vminfo2 |  select-string -pattern 'label:','os:','vcpu_count','ram','disk','^main_ip','location','power','SUBID:'   
        [array]$vms = @()
        $empty = ''
        switch ($vminfofiltered.Count) {
            0  {write-host 'No VMs found'}
            9  {$vmspassed = $vminfofiltered,$empty }
            18 {$vmspassed = $vminfofiltered[0..8],$vminfofiltered[9..17]}
            27 {$vmspassed = $vminfofiltered[0..8],$vminfofiltered[9..17],$vminfofiltered[18..26]}
            36 {$vmspassed = $vminfofiltered[0..8],$vminfofiltered[9..17],$vminfofiltered[18..26],$vminfofiltered[27..35]}
            45 {$vmspassed = $vminfofiltered[0..8],$vminfofiltered[9..17],$vminfofiltered[18..26],$vminfofiltered[27..35],$vminfofiltered[36..44]}
            54 {$vmspassed = $vminfofiltered[0..8],$vminfofiltered[9..17],$vminfofiltered[18..26],$vminfofiltered[27..35],$vminfofiltered[36..44],$vminfofiltered[45..53]}
        }  #switch 
    #for each collection in the collection of vmspassed find the label and other params,then filter the first parts
            foreach ($vm in $vmspassed) {
                if ($vm) {
                          $vmlabel = $vm  | where-object {$_ -like 'label:*'}
                          $vmlabel = ($vmlabel).ToString($vmlabel)
                          $vmlabel = $vmlabel.substring(6)
                              $vmos = $vm  | where-object {$_ -like 'os:*'}
                              $vmos = ($vmos).ToString($vmos)
                              $vmos = $vmos.substring(3)
                                  $vmvcpu = $vm  | where-object {$_ -like 'vcpu_count:*'}
                                  $vmvcpu = ($vmvcpu).ToString($vmvcpu)
                                  $vmvcpu = $vmvcpu.substring(11)
                                      $vmdisk = $vm  | where-object {$_ -like 'disk:*'}
                                      $vmdisk = ($vmdisk).ToString($vmdisk)
                                      $vmdisk = $vmdisk.substring(5)
                                          $vmram = $vm  | where-object {$_ -like 'ram:*'}
                                          $vmram = ($vmram).ToString($vmram)
                                          $vmram = $vmram.substring(4)
                                              $vmloc = $vm  | where-object {$_ -like 'location:*'}
                                              $vmloc = ($vmloc).ToString($vmloc)
                                              $vmloc = $vmloc.substring(9)
                                                  $vmpower = $vm  | where-object {$_ -like 'power_status:*'}
                                                  $vmpower = ($vmpower).ToString($vmpower)
                                                  $vmpower = $vmpower.substring(13)
                                                    $vmsubid = $vm | where-object {$_ -like '*SUBID:*'}
                                                    $vmsubid = $vmsubid -split ':'
                                                    $vmsubid = $vmsubid[2]
                                   $vmobj =  [pscustomobject]@{name = $vmlabel;
                                                                    os = $vmos;
                                                                    VCPU = $vmvcpu;
                                                                    ram = $vmram;
                                                                    disk = $vmdisk;
                                                                    location = $vmloc;
                                                                    Power = $vmpower;
                                                                    SUBID = $vmsubid;
                                                                    API = $api
                                                                    }#hashtable
            
                                $vms += $vmobj
                                }#foreach
                    }#if
                   
                  $vms
                    }#parameterset switch scriptblock 
                            'SingleVM' {
                                [pscustomobject]$vms = @()
                                  foreach ($indname in $name) {
                                    $vmobj = get-vultrvm -api $api | where-object name -eq $indname
                                    $vms += $vmobj
                                 }#foreach
                                 $vms
                             }#parameterset switch
   }#main switch   
}#function
function Restart-VultrVM {
     <#
.Synopsis
   Allows you to restart virtual machines on Vultr.com
.DESCRIPTION
   This function has a parameterset that will accept pipeline input from Get-VultrVM.
   You can also just specify your virtual machine with this function's parameter set as well.
   You should get your Vultr API from your vultr portal and store it in a variable like $apivar
   Please see the two examples in the get-help Restart-VultrVM -full
.EXAMPLE
   Get-VultrVM -name 'VM1','VM2' -API $apivar | Restart-VultrVM
.EXAMPLE
   Restart-VultrVM -name 'VM1' -API $apivar
#>
[cmdletbinding()]
    param (
        [parameter(Mandatory = $true,
                   Position = 0,
                   ParameterSetName= 'single')]
        [array]$name,
        [parameter(Mandatory = $true,
                   Position = 1,
                   ParameterSetName= 'single')]
        [string]$api,
        [parameter(Position = 0,
                   ValueFromPipeline = $true,
                   ParameterSetName = 'Input')]
        [PSobject[]]$InputObject
        )#param
process {
        $objcol = @()

    switch ($PSCmdlet.ParameterSetName) {

    'single' {
                        
                    $list = invoke-webrequest -uri https://api.vultr.com/v1/server/list?api_key=$api -Method get 
                    Start-Sleep -Seconds 1
                    $list2 = $list.Content -split ',' 
                    $list3 = $list2.replace("`"",'')
                    $list4 = $list3 | Select-String -Pattern '^label:','SUBID:'
                    $list5 = $list4 -split ':'
                    $empty = ''
                        switch ($list5.Count) {

                        0  {write-host 'No VMs found'}
                        5  {$vmls = $list5,$empty }
                        10 {$vmls = $list5[0..4],$list5[5..9]}
                        15 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14]}
                        20 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19]}
                        30 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24]}
                        35 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24],$list5[25..29]}
                    }  #switch 


                    $atran = @()

                    foreach ($vm in $vmls) {

                        if ($vm) {

                            $label = $vm[4]
                            $SUBID = $vm[2]

            
                                   $btran =  [pscustomobject]@{name = $label;
                                                               subid = $SUBID}


            
                            $atran += $btran
                        }#if
                    }#foreach
                 foreach ($named in $name)  {
                            $vmtoreboot = $atran | where-object name -eq $named | select-object -ExpandProperty subid
                            $body = "SUBID=$vmtoreboot"
                            $uri = "https://api.vultr.com/v1/server/reboot?api_key=$api"
                            Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
                            write-warning "$Named has been rebooted." 
                            Start-Sleep -Seconds 1
                }#foreach           

            }#single switch
                'input' {
                Start-Sleep -Seconds 1
                $named = $InputObject | Select-Object -expandproperty name
                    $subid2 = $InputObject | Select-Object -expandproperty SUBID  
                    $api = $InputObject | Select-Object -ExpandProperty api
                    $body = "SUBID=$subid2"
                     $uri = "https://api.vultr.com/v1/server/reboot?api_key=$api"
                            Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
                            write-warning "$Named has been rebooted." 
                            Start-Sleep -Seconds 1       
                }#input switch
                
            }#full switch
            }#process
           
}#function
function Start-VultrVM {
     <#
.Synopsis
   Allows you to start or restart virtual machines on Vultr.com
.DESCRIPTION
   This function has a parameterset that will accept pipeline input from Get-VultrVM.
   You can also just specify your virtual machine with this function's parameter set as well.
   You should get your Vultr API from your vultr portal and store it in a variable like $apivar
   Please see the two examples in the get-help Start-VultrVM -full
.EXAMPLE
   Get-VultrVM -name 'VM1','VM2' -API $apivar | Start-VultrVM
.EXAMPLE
   Start-VultrVM -name 'VM1' -API $apivar
#>
    param (

        [parameter(Mandatory = $true,
                   ParameterSetName = 'Single',
                   Position = 0)]
        [array]$name,

        [parameter(Mandatory = $true,
                   ParameterSetName = 'Single',
                   Position = 1)]
        [string]$api,
        
        [parameter(ValueFromPipeline = $true,
                   ParameterSetName = 'Input')]
        $InputObject
    )#param

  
 Process {

    switch ($PSCmdlet.ParameterSetName) {
        'Single' {

        
$list = invoke-webrequest -uri https://api.vultr.com/v1/server/list?api_key=$api -Method get 

    $list2 = $list.Content -split ',' 
    $list3 = $list2.replace("`"",'')
    $list4 = $list3 | Select-String -Pattern '^label:','SUBID:'
    $list5 = $list4 -split ':'

    $empty = ''

        switch ($list5.Count) {

        0  {write-host 'No VMs found'}
        5  {$vmls = $list5,$empty }
        10 {$vmls = $list5[0..4],$list5[5..9]}
        15 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14]}
        20 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19]}
        30 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24]}
        35 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24],$list5[25..29]}
    }  #switch 


    $atran = @()

    foreach ($vm in $vmls) {

        if ($vm) {

            $label = $vm[4]
            $SUBID = $vm[2]

            
                   $btran =  [pscustomobject]@{name = $label;
                                               subid = $SUBID}


            
            $atran += $btran
        }#if
    }#foreach

    foreach ($namedvm in $Name) {
        Start-Sleep -Seconds 1  
        $vmtostart = $atran | where-object name -eq $namedvm | select-object -ExpandProperty subid
        $body = "SUBID=$vmtostart"
        $uri = "https://api.vultr.com/v1/server/start?api_key=$api"
        Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
        write-warning "$Namedvm has been started."     
        Start-Sleep -Seconds 1  
    }#foreach
      }#single switch

        'Input' {
            Start-Sleep -Seconds 1
            $name = $InputObject | Select-Object -ExpandProperty name
            $api = $InputObject | Select-Object -ExpandProperty api
            $subid = $InputObject | Select-Object -ExpandProperty subid
            $body = "SUBID=$subid"
             $uri = "https://api.vultr.com/v1/server/start?api_key=$api"
                Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
                write-warning "$name has been stopped."    
                Start-Sleep -Seconds 1  
        }#input switch

        }#main switch
    }#process
}#function
function Stop-VultrVM {
     <#
.Synopsis
   Allows you to stop virtual machines on Vultr.com
.DESCRIPTION
   This function has a parameterset that will accept pipeline input from Get-VultrVM.
   You can also just specify your virtual machine with this function's parameter set as well.
   You should get your Vultr API from your vultr portal and store it in a variable like $apivar
   Please see the two examples in the get-help Stop-VultrVM -full
.EXAMPLE
   Get-VultrVM -name 'VM1','VM2' -API $apivar | Stop-VultrVM
.EXAMPLE
   Stop-VultrVM -name 'VM1' -API $apivar
#>
    param (
        [parameter(Mandatory = $true,
                   ParameterSetName = 'Single',
                   Position = 0)]
        [array]$name,

        [parameter(Mandatory = $true,
                   Position = 1,
                   ParameterSetName = 'Single')]
        [string]$api,
        [parameter(ValueFromPipeline = $true,
                   ParameterSetName = 'Input')]
        [psobject[]]$InputObject
    )#param

  
 Process {


 switch ($PSCmdlet.ParameterSetName) {

 'Single' {
$list = invoke-webrequest -uri https://api.vultr.com/v1/server/list?api_key=$api -Method get 

    $list2 = $list.Content -split ',' 
    $list3 = $list2.replace("`"",'')
    $list4 = $list3 | Select-String -Pattern '^label:','SUBID:'
    $list5 = $list4 -split ':'

    $empty = ''

        switch ($list5.Count) {
            0  {write-host 'No VMs found'}
            5  {$vmls = $list5,$empty }
            10 {$vmls = $list5[0..4],$list5[5..9]}
            15 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14]}
            20 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19]}
            30 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24]}
            35 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24],$list5[25..29]}
        }  #switch 


    $atran = @()

    foreach ($vm in $vmls) {

        if ($vm) {

            $label = $vm[4]
            $SUBID = $vm[2]

            
                   $btran =  [pscustomobject]@{name = $label;
                                               subid = $SUBID}


            
            $atran += $btran
        }#if
    }#foreach

    foreach ($namedvm in $Name) {
        Start-Sleep -Seconds 1
        $vmtostop = $atran | where-object name -eq $namedvm | select-object -ExpandProperty subid
        $body = "SUBID=$vmtostop"
        $uri = "https://api.vultr.com/v1/server/halt?api_key=$api"
        Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
        write-warning "$Namedvm has been stopped."    
        Start-Sleep -Seconds 1   
    }#foreach
    }#single switch scriptblock

    'Input' {
    Start-Sleep -Seconds 1
    $name = $InputObject | Select-Object -ExpandProperty name
    $api = $InputObject | Select-Object -ExpandProperty api
    $subid = $InputObject | Select-Object -ExpandProperty subid
    $body = "SUBID=$subid"
     $uri = "https://api.vultr.com/v1/server/halt?api_key=$api"
        Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
        write-warning "$name has been stopped."    
        Start-Sleep -Seconds 1  
    }#input switch scriptblock
    }#switch parameterset
    }#process
}#function
function Remove-VultrVM {
     <#
.Synopsis
   Allows you to destroy virtual machines on Vultr.com
.DESCRIPTION
   This function has a parameterset that will accept pipeline input from Get-VultrVM.
   You can also just specify your virtual machine with this function's parameter set as well.
   You should get your Vultr API from your vultr portal and store it in a variable like $apivar
   Please see the two examples in the get-help Restart-VultrVM -full
.EXAMPLE
   Get-VultrVM -name 'VM1','VM2' -API $apivar | Remove-VultrVM
.EXAMPLE
   Remove-VultrVM -name 'VM1' -API $apivar
#>
    param (
        [parameter(Mandatory = $true,
                   ParameterSetName = 'Single',
                   Position = 0)]
        [array]$name,

        [parameter(Mandatory = $true,
                   Position = 1,
                   ParameterSetName = 'Single')]
        [string]$api,
        [parameter(ValueFromPipeline = $true,
                   ParameterSetName = 'Input')]
        [psobject[]]$InputObject
    )
Process {

switch ($PSCmdlet.ParameterSetName) {
'Single' {   

$list = invoke-webrequest -uri https://api.vultr.com/v1/server/list?api_key=$api -Method get 

    $list2 = $list.Content -split ',' 
    $list3 = $list2.replace("`"",'')
    $list4 = $list3 | Select-String -Pattern '^label:','SUBID:'
    $list5 = $list4 -split ':'

    $empty = ''

        switch ($list5.Count) {

        0  {write-host 'No VMs found'}
        5  {$vmls = $list5,$empty }
        10 {$vmls = $list5[0..4],$list5[5..9]}
        15 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14]}
        20 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19]}
        30 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24]}
        35 {$vmls = $list5[0..4],$list5[5..9],$list5[10..14],$list5[15..19],$list5[20..24],$list5[25..29]}
    }  #switch 


    $atran = @()

    foreach ($vm in $vmls) {

        if ($vm) {

            $label = $vm[4]
            $SUBID = $vm[2]

            
                   $btran =  [pscustomobject]@{name = $label;
                                               subid = $SUBID}


            
            $atran += $btran
        }#if
    }#foreach


        foreach ($namedvm in $Name) {
            Start-Sleep -Seconds 1   
            $vmtodestroy = $atran | where-object name -eq $namedvm | select-object -ExpandProperty subid
            $body = "SUBID=$vmtodestroy"
            $uri = "https://api.vultr.com/v1/server/destroy?api_key=$api"
            Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
            write-warning " $Namedvm has been removed." 
            Start-Sleep -Seconds 1    
    }#foreach
   
    }#switch single

    'Input' {
                Start-Sleep -Seconds 1
            $name = $InputObject | Select-Object -ExpandProperty name
            $api = $InputObject | Select-Object -ExpandProperty api
            $subid = $InputObject | Select-Object -ExpandProperty subid
            $body = "SUBID=$subid"
             $uri = "https://api.vultr.com/v1/server/destroy?api_key=$api"
                Invoke-WebRequest -uri $uri -Method post -body $body -ContentType 'application/x-www-form-urlencoded' | Out-Null
                write-warning "$name has been removed."    
                Start-Sleep -Seconds 1 

    }
     }#main switch
    }#process
} #function
function New-VultrVM {
     <#
.Synopsis
   Allows you to provision new virtual machines on Vultr.com
.DESCRIPTION
This cmdlet requires a name, location, os, planid and API.
For the name parameter: You can make one up.
For the location parameter, you can choose from:
Atlanta
Chicago
Dallas 
Los Angeles
Miami
New Jersey
Seattle
Silicon Valley
Amsterdam
Tokyo
London
Paris
Frankfurt
Sydney

For the OS parameter, you can choose from:
CentOS 6 x64
CentOS 6 i386
CentOS 5 x64
CentOS 5 i386
Centos 7 x64
Ubuntu 12.04 x64
Ubuntu 12.04 i386
Ubuntu 14.04 x64
Ubuntu 14.04 i386
Ubuntu 15.04 x64
Ubuntu 15.04 i386
Ubuntu 15.10 x64
Ubuntu 15.10 i386
Debian 7 x64
Debian 7 i386
Debian 8 x64
Debian 8 i386
FreeBSD 10.2 x64
CoreOS Stable
Windows 2012 R2 x64

For the planid parameter you should:
Use the Get-VultrPlanInRegion -Location $location function to get the available plans in your region.

For the API parameter you should:
Login to Vultr.com, go to your settings page, enable and copy your parameter into a var.

   Please see the two examples in the get-help Restart-VultrVM -full
.EXAMPLE
   I will show you two ways of creating a new VM.
   
 1. Make sure your API key is in a variable or in your clipboard.
 Get your planid with the function:
 Get-VultrPlanInRegion -Location 'Chicago"
 Remember the plan id and then fill out the function.
New-VultrVM -name 'test name' -location 'Chicago' -os 'CentOS 5 x64' -planid '93' -api $api

2. Filter your plan by vcpu, bandwidth, disk, disk type, ram, or even price. Then pipe the single plan to New-VultrVM
 Get-VultrPlanInRegion 'Chicago' | ? price -le 10 | ? disktype -eq SSD | New-VultrVM -name 'test vm 2' -location 'chicago' -os 'centos 5 x64' -api $api
#>
    param (
         [parameter(mandatory = $true)]
         [ValidateNotNullOrEmpty()]
         [string]$name,

         [parameter(mandatory = $true)]
         [ValidateNotNullOrEmpty()]
         [validateset(
        'Atlanta',
        'Chicago',
        'Dallas',
        'Los Angeles',
        'Miami',
        'New Jersey',
        'Seattle',
        'Silicon Valley',
        'Amsterdam',
        'Tokyo',
        'London',
        'Paris',
        'Frankfurt',
        'Sydney',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '12',
        '24',
        '25',
        '199')]
         $location,

         [parameter(mandatory = $true)]
         [ValidateNotNullOrEmpty()]
         [validateset(
        'CentOS 6 x64',
        'CentOS 6 i386',
        'CentOS 5 x64',
        'CentOS 5 i386',
        'Centos 7 x64',
        'Ubuntu 12.04 x64',
        'Ubuntu 12.04 i386',
        'Ubuntu 14.04 x64',
        'Ubuntu 14.04 i386',
        'Ubuntu 15.04 x64',
        'Ubuntu 15.04 i386',
        'Ubuntu 15.10 x64',
        'Ubuntu 15.10 i386',
        'Debian 7 x64',
        'Debian 7 i386',
        'Debian 8 x64',
        'Debian 8 i386',
        'FreeBSD 10.2 x64',
        'CoreOS Stable',
        'Windows 2012 R2 x64',
        '127',
        '147',
        '162',
        '163',
        '167',
        '128',
        '148',
        '160',
        '161',
        '191',
        '192',
        '209',
        '210',
        '139',
        '152',
        '193',
        '194',
        '140',
        '179',
        '124')]
         [string]$os, 

          [parameter(mandatory = $true,
          ValueFromPipelineByPropertyName = $true)]
          [ValidateNotNullOrEmpty()]
         $planid, 

         [parameter(mandatory = $true)]
         [ValidateNotNullOrEmpty()]
         [ValidateScript({
              if (-not (Test-Connection -Quiet -ComputerName 8.8.8.8 -Count 1))
                  {
                    throw "Your computer doesn't appear to have internet connectivity"
                  }
                  else
                  {
                    $true
              }
         })]
         [string]$api
  
    )#param

    if ($location -is [string]) {
        switch ($location) {
            'Atlanta'        {$location = '6'}
            'Chicago'        {$location = '2'}
            'Dallas'         {$location = '3'}
            'Los Angeles'    {$location = '5'}
            'Miami'          {$location = '6'}
            'New Jersey'     {$location = '1'}
            'Seattle'        {$location = '4'}
            'Silicon Valley' {$location = '12'}
            'Amsterdam'      {$location = '7'}
            'Tokyo'          {$location = '25'}
            'London'         {$location = '8'}
            'Paris'          {$location = '24'}
            'Frankfurt'      {$location = '9'}
            'Sydney'         {$location = '199'}
        }#switch
     }#if
  

           switch ($os) {
            'CentOS 6 x64'        {$osid = 127}
            'CentOS 6 i386'       {$osid = 147}
            'CentOS 5 x64'        {$osid = 162}
            'CentOS 5 i386'       {$osid = 163}
            'Centos 7 x64'        {$osid = 167}
            'Ubuntu 12.04 x64'    {$osid = 128}
            'Ubuntu 12.04 i386'   {$osid = 148}
            'Ubuntu 14.04 x64'    {$osid = 160}
            'Ubuntu 14.04 i386'   {$osid = 161}
            'Ubuntu 15.04 x64'    {$osid = 191}
            'Ubuntu 15.04 i386'   {$osid = 192}
            'Ubuntu 15.10 x64'    {$osid = 209}
            'Ubuntu 15.10 i386'   {$osid = 210}
            'Debian 7 x64'        {$osid = 139}
            'Debian 7 i386'       {$osid = 152}
            'Debian 8 x64'        {$osid = 193}
            'Debian 8 i386'       {$osid = 194}
            'FreeBSD 10.2 x64'    {$osid = 140}
            'CoreOS Stable'       {$osid = 179}
            'Windows 2012 R2 x64' {$osid = 124}
           }#switch




           



                    $body = "DCID=$location&VPSPLANID=$planid&OSID=$osid&label=$name"

                    $uri = "https://api.vultr.com/v1/server/create?api_key=$api"

                    $vultrpage = Invoke-WebRequest -uri $uri -Method post -body $body -SessionVariable vultrapi -ContentType 'application/x-www-form-urlencoded'

                    $newvm = [pscustomobject]@{name = $name;
                                               api = $api}
                        
                    Write-Warning "$name has been created."


}#function
function Get-VultrRegionAvailability {
    [cmdletbinding()]
        param(
            [parameter(Mandatory = $true)]
            [ValidateNotNullOrEmpty()]
            $location
        )#param

                switch ($location) {
            'Atlanta'        {$location = '6'}
            'Chicago'        {$location = '2'}
            'Dallas'         {$location = '3'}
            'Los Angeles'    {$location = '5'}
            'Miami'          {$location = '6'}
            'New Jersey'     {$location = '1'}
            'Seattle'        {$location = '4'}
            'Silicon Valley' {$location = '12'}
            'Amsterdam'      {$location = '7'}
            'Tokyo'          {$location = '25'}
            'London'         {$location = '8'}
            'Paris'          {$location = '24'}
            'Frankfurt'      {$location = '9'}
            'Sydney'         {$location = '199'}
        }#switch
         
         $avail = invoke-webrequest -method Get -uri https://api.vultr.com/v1/regions/availability?DCID=$location
         $avail = $avail.content
         $avail = $avail.replace('[','')
         $avail = $avail.replace(']','')
         $avail = $avail -split ','
         [array]$avail = $avail -replace '\[','' -replace '\]',''
        

         $availcol = @()

         foreach ($availplan in $avail) {
             $availobj = [pscustomobject]@{planid = $availplan}
             $availcol += $availobj
         }#foreach


       $availcol
            


            
      


}#function
function Get-VUltrPlanList {
    [cmdletbinding()]


            $plans = Invoke-WebRequest -Method Get -uri https://api.vultr.com/v1/plans/list
            $Plans = $plans.Content
            $plans = $plans.Split(']}')
            $plans = $plans -replace "`"",''
            $plans = $plans -replace '^,',''
            $plans = $plans -replace '^\d{1,3}:{',''
            $plans = $plans -replace '\[',''
            $plans = $plans -split ",`""
            $plans = $plans -replace 'windows:false,',''
            $plans = $plans -replace ',name','-name' -replace 'RAM,','RAM-' -replace 'BW,VCPU','BW-vcpu' -replace ',ram','-ram' -replace ',disk','-disk' -replace ',bandwidth','-bandwidth' -replace ',price','-price' -replace ',plan','-plan' -replace ',avail','-avail'
            $plans = $Plans -split '-'
            [array]$plans = $plans | Select-String -Pattern 'vpsplanid','name','vcpu','ram','disk','bandwidth_gb','price','plan_type','available'

            $plancol = @()

            $b = 1

            [array]$array = 'top'

            $array += $plans

            for ($a = 0; $a -le (($array.count - 1) / 9 - 1); $a++) {
        
                $planfilter =  $plans[$b..($b = $b + 8)]

                    $planid = $planfilter | where-object {$_ -like '*vpsplanid*'} 
                    $Planid = $planid -split ':'
                    $planid = $planid[1]
                    $planid = $planid -as [int]
                        $bandwidth = $planfilter | where-object {$_ -like '*bandwidth_gb:*'}
                        $bandwidth = $bandwidth -split ':'
                        $bandwidth = $bandwidth[1]
                        $bandwidth = $bandwidth -as [int]
                            $disktype = $planfilter | where-object {$_ -like '*plan_type*'}
                            $disktype = $disktype -split ':'
                            $disktype = $disktype[1]
                                $ram = $planfilter | where-object {$_ -like '*ram:*'}
                                $ram = $ram -split ':'
                                $ram = $ram[1]
                                $ram = $ram -as [int]
                                    $price = $planfilter | where-object {$_ -like 'price*'}
                                    $price = $price -split ':'
                                    $price = $price[1]
                                    $price = $price -as [int]
                                        [array]$available2 = @()
                                        $available = $planfilter | where-object {$_ -like 'available*'}
                                        $available = $available -split ':'
                                        $available = $available[1]
                                        $available = $available -split ','
                                        foreach ($avail in $available) {
                                        $avail = $avail -as [int]
                                        $available2 += $avail
                                        }#foreach
                                            $disk = $planfilter | where-object {$_ -like 'disk:*'}
                                            $disk = $disk -split ':'
                                            $disk = $disk[1]
                                            $disk = $disk -as [int]
                                                $vcpu = $planfilter | Where-Object {$_ -like 'vcpu_count*'}
                                                $vcpu = $vcpu -split ':'
                                                $vcpu = $vcpu[1]
                                                $vpcu = $vcpu -as [int]


                $planobj = [pscustomobject]@{PLANID = $planid;
                                            vcpu = $vcpu;
                                            BandwidthGB = $bandwidth;
                                            disktype = $disktype;
                                            diskGB = $disk;
                                            ramMB = $ram;
                                            price = $price;
                                            location = $available2
                                            }
                $plancol += $planobj
            }#for


            $plancol
        
}#function
function Get-VultrPlanInRegion {
Param(

$Location

)#param

$plans = Get-VultrRegionAvailability -location $location | select-object -ExpandProperty  PLANID 
    foreach ($plan in $plans) {
        get-VUltrPlanList | where-object planid -eq $plan
    }#foreach
}#function

#Created by Vic @ http://techstat.net 

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           