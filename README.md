Visit http://techstat.net/vultr-com-module-for-powershell-create-start-stop-restart-and-destroy-virtual-machines/
For more information on usage and screenshots.
<br>
Basic usage:
<br>
Extract the Vultr folder to<br>
C:\Program Files\WindowsPowerShell\Modules<br>
Your final path to the .psm1 file should be.<br><br>
C:\Program Files\WindowsPowerShell\Modules\Vultr\Vultr.psm1
Enable API use from Vultr.com in settings, copy your API key, put your API key into a powershell variable like so<br>
$api = 'myapikeyhere'<br>
Now you are ready to use the vultr module.<br>
Get-VultrVM -api $api will get all your VMs<br>
You can also pass VMs to other commands via the pipeline like so.<br>
Get-VultrVM -name 'myvm' -api $api | Restart-VultrVM<br>

You can also create VMs by using Get-VultrPlanInRegion -location 'MyDatacenterHere'<br>
EX:<br>
Get-VultrPlanInRegion -location 'chicago' | where-object disktype -eq ssd | where-object price -eq 10 |<br>
New-VultrVM -location 'chicago' os 'centos 5 x64' -name 'myname' -api $api<br>
<br>
All the functions can also be used individualy if you don't want to pipe to other commands<br>
EX:<br>
<br>
Stop-VUltrVM -name 'myvm' -api $api<br>
